package com.acme.bankapp.apps.bank.services;

import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.exceptions.SendEmailException;

import java.io.Serializable;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Kate on 23.04.2015.
 */
public class EmailService implements Serializable {
    private ConcurrentLinkedQueue<Email> queue;
    private volatile boolean stopped;

    public EmailService() {
        queue = new ConcurrentLinkedQueue<>();
        stopped = true;
    }

    public void sendModificationEmail(Client client, String eAddreess, String message) throws SendEmailException {
        if (!stopped) {
            addEmailToQueue( new Email(client, eAddreess, message));
        }
        else {
            throw new SendEmailException("EmailService is closed now. Thread name is "+ Thread.currentThread().getName());
        }
    }

    public void open() {
        if(stopped) {
            stopped = false;
            Thread thread = new Thread(() -> {
                while (!stopped) {
                    if (!queue.isEmpty()) {
                        sendEmail(queue.poll());
                    }
                }
                queue.forEach(this::sendEmail);
            });
            thread.start();
        }
    }

    public void close() {
        stopped = true;
    }

    private void addEmailToQueue(Email email){
        queue.add(email);
    }

    private void sendEmail(Email email){
        System.out.println(email.getMessage() + " here " + Thread.currentThread().getName());
    }

}
