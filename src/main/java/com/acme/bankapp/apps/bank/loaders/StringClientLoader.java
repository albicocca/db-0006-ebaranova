package com.acme.bankapp.apps.bank.loaders;

import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.exceptions.BankException;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;

/**
 * Created by kate on 26.04.15.
 */
public interface StringClientLoader {
    Client loadClient(String line) throws BankException;
}
