package com.acme.bankapp.apps.bank.loaders;

import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;
import com.acme.bankapp.exceptions.bank.WriteBankToFileException;

import java.io.*;

/**
 * Created by kate on 26.04.15.
 */
public class SerBankDAO implements BankLoader, BankSaver {

    private String filename;

    public SerBankDAO(String filename){
        this.filename = filename;
    }

    @Override
    public Bank loadBank() throws ReadBankFromFileException {
        try(ObjectInputStream objectInputStream = new ObjectInputStream( new FileInputStream(filename))) {
            return (Bank) objectInputStream.readObject();
        } catch (Exception e) {
            throw new ReadBankFromFileException(e);
        }
    }

    @Override
    public void saveBank(Bank bank) throws WriteBankToFileException {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(filename))){
            objectOutputStream.writeObject(bank);
        } catch (IOException e) {
            throw new WriteBankToFileException(e);
        }
    }
}
