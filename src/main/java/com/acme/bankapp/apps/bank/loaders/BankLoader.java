package com.acme.bankapp.apps.bank.loaders;

import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.exceptions.BankException;
import com.acme.bankapp.exceptions.ReadBankFromDBException;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;

/**
 * Created by kate on 26.04.15.
 */
public interface BankLoader {
    Bank loadBank() throws BankException;
}
