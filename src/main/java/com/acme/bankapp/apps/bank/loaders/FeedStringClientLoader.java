package com.acme.bankapp.apps.bank.loaders;

import com.acme.bankapp.domain.account.Account;
import com.acme.bankapp.domain.account.impl.CheckingAccount;
import com.acme.bankapp.domain.account.impl.SavingAccount;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.domain.client.Gender;
import com.acme.bankapp.exceptions.account.NegativeArgumentAccountConstructor;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kate on 26.04.15.
 */
public class FeedStringClientLoader implements StringClientLoader {

    private static final String ACCOUNT_TYPE = "accounttype";
    private static final String ACCOUNT_TYPE_CHECK = "c";
    private static final String ACCOUNT_TYPE_SAVE = "s";
    private static final String BALANCE = "balance";
    private static final String OVERDRAFT = "overdraft";
    private static final String NAME = "name";
    private static final String GENDER = "gender";
    private static final String GENDER_MALE = "m";
    private static final String GENDER_FEMALE = "f";

    @Override
    public Client loadClient(String line) throws ReadBankFromFileException {
        Client client;
        Account account;

        try {
            String[] fields = line.split(";");
            Map<String, String> values = new HashMap<>();
            for (String field : fields) {
                values.put(
                        field.split("=")[0],
                        field.split("=")[1]
                );
            }
            account = createAccount(values.get(ACCOUNT_TYPE), values.get(BALANCE), values.get(OVERDRAFT));
            client = createClient(values.get(NAME), values.get(GENDER), account, line);
        } catch (NegativeArgumentAccountConstructor | NumberFormatException e) {
            throw new ReadBankFromFileException(e);
        } catch (NullPointerException e){
            throw new ReadBankFromFileException("Not enough info in line: "+ line, e);
        }
        return client;
    }


    private Client createClient(String name, String gender_str, Account account, String line) throws ReadBankFromFileException {
        Client client;
        Gender gender;
        switch (gender_str) {
            case GENDER_MALE:
                gender = Gender.MALE;
                break;
            case GENDER_FEMALE:
                gender = Gender.FEMALE;
                break;
            default: throw new ReadBankFromFileException("Unknown gender in string: "+  line);
        }
        client = new Client(name, gender, account);
        return client;
    }

    private Account createAccount(String accountType, String balance, String overdraft) throws NegativeArgumentAccountConstructor, NumberFormatException, ReadBankFromFileException {
        Account account;
        switch (accountType){
            case ACCOUNT_TYPE_CHECK:
                account = new CheckingAccount(Double.parseDouble(balance), Double.parseDouble(overdraft));
                break;
            case ACCOUNT_TYPE_SAVE:
                account = new SavingAccount(Double.parseDouble(balance));
                break;
            default: throw new ReadBankFromFileException("Unknown type of account: "+ accountType);
        }
        return  account;
    }

    public static void main(String[] args) throws NegativeArgumentAccountConstructor, ReadBankFromFileException {
        new FeedStringClientLoader().loadClient("balance=1000;overdraft=50000;name=John;gender=wtf");
    }
}
