package com.acme.bankapp.apps.bank.loaders;

import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.exceptions.bank.WriteBankToFileException;

/**
 * Created by kate on 26.04.15.
 */
public interface BankSaver {
    void saveBank(Bank bank) throws WriteBankToFileException;
}
