package com.acme.bankapp.apps.bank;

import com.acme.bankapp.apps.bank.loaders.SerBankDAO;
import com.acme.bankapp.domain.account.impl.CheckingAccount;
import com.acme.bankapp.domain.account.impl.SavingAccount;
import com.acme.bankapp.domain.bank.*;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.domain.client.Gender;
import com.acme.bankapp.exceptions.BankException;
import com.acme.bankapp.exceptions.account.NegativeArgumentAccountConstructor;
import com.acme.bankapp.exceptions.account.NotEnoughFundsException;
import com.acme.bankapp.exceptions.bank.WriteBankToFileException;
import com.acme.bankapp.exceptions.client.ClientExistsException;
import com.acme.bankapp.apps.bank.loaders.BankDAO;
import com.acme.bankapp.apps.bank.loaders.FileBankDAO;
import com.acme.bankapp.apps.bank.services.BankService;
import com.acme.bankapp.tcp.server.BankServer;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class BankApplication {

    private static BankService bankService = new BankService();

    public static void main(String[] args) throws BankException, NegativeArgumentAccountConstructor,FileNotFoundException {
        BankApplication bankApplication = new BankApplication();
        List<String> argList = Arrays.asList(args);
        Iterator<String> iterator = argList.listIterator();
        Bank bank = null;
        String arg;
        String filename;
        while(iterator.hasNext()){
            arg = iterator.next();
            switch (arg){
                case "-loadfeed":
                    filename = iterator.next(); // TODO: что если пользователь не указал - тогда не получится взять след значение (придумать экзепшены и их обработку)ж
                    bank = bankService.loadBank(new FileBankDAO(filename));
                    break;
                case "-loadser":
                    filename = iterator.next(); // TODO: что если пользователь не указал - тогда не получится взять след значение (придумать экзепшены и их обработку)ж
                    bank = bankService.loadBank(new SerBankDAO(filename));
                    bankService.printAllInfo(bank);
                    break;
                case "-db":
                    bank = bankService.loadBank(new BankDAO());
                    break;
                case "-server":
                    try {
                        if (bank == null) {
                            bank = bankApplication.startBankFromSample();
                            bankApplication.modifyBank(bank);
                        }
                        bank.addClient(new Client("WithOutEmail",Gender.FEMALE));
                        BankServer bankServer = new BankServer();
                        bank.addClient(new Client("With email", Gender.FEMALE));
                        bankServer.start(bank);
                    } catch (ClientExistsException e) {
                        e.printStackTrace();
                    } catch (BankException e) {
                        System.err.println(e);
                    }
                    break;
                default:
                    System.out.println("Use args  -loadfeed, -loadser, -server to say app what to do ");
            }
        }
        System.out.println();
        System.out.println("End app");

    }

        public Bank startBankFromSample() throws NegativeArgumentAccountConstructor, ClientExistsException, WriteBankToFileException {
        Bank bank = new Bank();
        int COUNT_OF_CLIENTS = 1000;
        Client client;
        for (int i=0; i< COUNT_OF_CLIENTS/2; i++){

            client= new Client("Kate-"+i, Gender.FEMALE);
            client.addAccount(new SavingAccount(1000));
            bank.addClient(client);

            client =new Client("Peter-"+i, Gender.MALE);
            client.addAccount( new CheckingAccount(1000,5000));
            bank.addClient(client);

        }
        bankService.saveBank(bank, new SerBankDAO("bank.ser"));
        return bank;
    }
    public void modifyBank(Bank bank) throws NotEnoughFundsException, WriteBankToFileException {
        bank.getClients().get(0).getAccounts().deposit(1000);
        bank.getClients().get(1).getAccounts().withdraw(1000);
        bank.getClients().get(2).getAccounts().deposit(1000);
        bank.getClients().get(3).getAccounts().withdraw(1000);
        bankService.saveBank(bank, new SerBankDAO("bank.ser"));
    }
}
