package com.acme.bankapp.apps.bank.loaders;

import com.acme.bankapp.domain.account.Account;
import com.acme.bankapp.domain.account.impl.CheckingAccount;
import com.acme.bankapp.domain.account.impl.SavingAccount;
import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.domain.client.Gender;
import com.acme.bankapp.exceptions.ReadBankFromDBException;
import com.acme.bankapp.exceptions.account.NegativeArgumentAccountConstructor;
import com.acme.bankapp.exceptions.client.ClientExistsException;

import java.sql.*;

/**
 * Created by kate on 26.04.15.
 */
public class BankDAO implements BankLoader {
    private String DRIVER = "org.apache.derby.jdbc.ClientDriver";
    private String DBURI = "jdbc:derby://localhost/bankapp;create=true";

    private static final String ACCOUNT_TYPE_CHECK = "c";
    private static final String ACCOUNT_TYPE_SAVE = "s";
    private static final String GENDER_MALE = "m";
    private static final String GENDER_FEMALE = "f";

    public BankDAO() {

    }

    public BankDAO(String DRIVER, String DBURI) {
        this.DRIVER = DRIVER;
        this.DBURI = DBURI;
    }

    @Override
    public Bank loadBank() throws ReadBankFromDBException {
        Bank bank = new Bank();
        try {
            Class.forName(DRIVER);
            Connection conn = DriverManager.getConnection(DBURI);

            PreparedStatement statement = conn.prepareStatement("SELECT NAME, GENDER, ACCOUNTTYPE, BALANCE, OVERDRAFT FROM CLIENTS");
            ResultSet cursor = statement.executeQuery();

            while (cursor.next()){
                bank.addClient(loadClient(cursor));
            }

        } catch (ClassNotFoundException | SQLException | NegativeArgumentAccountConstructor | ClientExistsException e) {
            throw  new ReadBankFromDBException(e);
        }
        return  bank;
    }

    private Client loadClient(ResultSet cursor) throws SQLException, ReadBankFromDBException, NegativeArgumentAccountConstructor, ClientExistsException {
        Account account = createAccount(
                cursor.getString("accounttype"),
                cursor.getDouble("balance"),
                cursor.getDouble("overdraft")
        );
        return createClient(
                cursor.getString("name"),
                cursor.getString("gender"),
                account);
    }

    private Client createClient(String name, String gender_str, Account account) throws ReadBankFromDBException {
        Client client;
        Gender gender;
        switch (gender_str) {
            case GENDER_MALE:
                gender = Gender.MALE;
                break;
            case GENDER_FEMALE:
                gender = Gender.FEMALE;
                break;
            default: throw new ReadBankFromDBException("Unknown gender in db: " + gender_str);
        }
        client = new Client(name, gender, account);
        return client;
    }

    private Account createAccount(String accounttype, double balance, double overdraft) throws NegativeArgumentAccountConstructor, ReadBankFromDBException {
        Account account;
        switch (accounttype){
            case ACCOUNT_TYPE_CHECK:
                account = new CheckingAccount(balance, overdraft);
                break;
            case ACCOUNT_TYPE_SAVE:
                account = new SavingAccount(balance);
                break;
            default: throw new ReadBankFromDBException("Unknown type of account: "+ accounttype);
        }
        return  account;
    }
}
