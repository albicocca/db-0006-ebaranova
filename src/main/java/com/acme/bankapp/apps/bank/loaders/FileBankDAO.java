package com.acme.bankapp.apps.bank.loaders;

import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;
import com.acme.bankapp.apps.bank.services.BankService;

import java.io.*;

/**
 * Created by kate on 26.04.15.
 */
public class FileBankDAO implements BankLoader {
    private InputStream inputStream;
    private StringClientLoader stringClientLoader;

    public FileBankDAO(String filename) throws FileNotFoundException {
        this.inputStream = new FileInputStream(filename);
        stringClientLoader = new FeedStringClientLoader();
    }

    public FileBankDAO(InputStream inputStream){
        this.inputStream = inputStream;
        stringClientLoader = new FeedStringClientLoader();
    }

    public void setStringClientLoader(StringClientLoader stringClientLoader) {
        this.stringClientLoader = stringClientLoader;
    }

    @Override
    public Bank loadBank() throws ReadBankFromFileException {
        Bank bank = new Bank();
        String line;
        try(BufferedReader in = new BufferedReader(new InputStreamReader(inputStream))) {
            while ((line=in.readLine())!=null){
                bank.addClient(stringClientLoader.loadClient(line));
            }
        } catch (Exception e) {
            throw new ReadBankFromFileException(e);
        }
        return bank;
    }

    public static void main(String[] args) throws FileNotFoundException, ReadBankFromFileException {
        new BankService().printAllInfo(new FileBankDAO("feed").loadBank());
    }
}
