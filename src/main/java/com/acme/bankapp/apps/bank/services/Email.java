package com.acme.bankapp.apps.bank.services;

import com.acme.bankapp.domain.client.Client;

/**
 * Created by Kate on 23.04.2015.
 */
public class Email {
    private Client client;
    private String email;

    private String message;

    public Email(Client client, String email, String message) {
        this.client = client;
        this.email = email;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getEmail() {
        return email;
    }

    public Client getClient() {
        return client;
    }
}
