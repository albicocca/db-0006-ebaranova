package com.acme.bankapp.apps.bank.services;

import com.acme.bankapp.apps.bank.loaders.BankLoader;
import com.acme.bankapp.apps.bank.loaders.BankSaver;
import com.acme.bankapp.apps.bank.loaders.FeedStringClientLoader;
import com.acme.bankapp.domain.bank.*;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.exceptions.BankException;
import com.acme.bankapp.exceptions.client.ClientExistsException;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;
import com.acme.bankapp.exceptions.bank.WriteBankToFileException;

public class BankService {



    public void  saveBank(Bank bank, BankSaver bankSaver) throws WriteBankToFileException {
        bankSaver.saveBank(bank);
    }

    public void printAllInfo(Bank bank){
        bank.getClients().forEach(System.out::println);
    }

    public Bank loadBank(BankLoader bankLoader) throws BankException {
        return bankLoader.loadBank();
    }

    public void addClient(Bank bank, Client client) throws ClientExistsException {
            bank.addClient(client);
    }


    public void addClient(Bank bank, String client) throws ClientExistsException, ReadBankFromFileException {
        bank.addClient(new FeedStringClientLoader().loadClient(client));
    }

}
