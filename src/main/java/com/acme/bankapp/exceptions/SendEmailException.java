package com.acme.bankapp.exceptions;

import com.acme.bankapp.exceptions.BankException;

/**
 * Created by Kate on 24.04.2015.
 */
public class SendEmailException extends BankException {
    public SendEmailException() {
        super();
    }

    public SendEmailException(String message) {
        super(message);
    }

    public SendEmailException(String message, Throwable cause) {
        super(message, cause);
    }

    public SendEmailException(Throwable cause) {
        super(cause);
    }

    public SendEmailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
