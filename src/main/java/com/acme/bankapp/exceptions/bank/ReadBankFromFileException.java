package com.acme.bankapp.exceptions.bank;


import com.acme.bankapp.exceptions.BankException;

/**
 * Created by Kate on 21.04.2015.
 */
public class ReadBankFromFileException extends BankException {
    public ReadBankFromFileException() {
        super();
    }

    public ReadBankFromFileException(String message) {
        super(message);
    }

    public ReadBankFromFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReadBankFromFileException(Throwable cause) {
        super(cause);
    }

    protected ReadBankFromFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}