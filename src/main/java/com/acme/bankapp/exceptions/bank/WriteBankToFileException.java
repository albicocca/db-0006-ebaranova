package com.acme.bankapp.exceptions.bank;

import com.acme.bankapp.exceptions.BankException;

/**
 * Created by Kate on 21.04.2015.
 */
public class WriteBankToFileException extends BankException{
    public WriteBankToFileException() {
        super();
    }

    public WriteBankToFileException(String message) {
        super(message);
    }

    public WriteBankToFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public WriteBankToFileException(Throwable cause) {
        super(cause);
    }

    protected WriteBankToFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
