package com.acme.bankapp.exceptions.client;

import com.acme.bankapp.exceptions.BankException;

/**
 * Created by Kate on 20.04.2015.
 */
public class ClientExistsException extends BankException {
    public ClientExistsException() {
        super();
    }

    public ClientExistsException(String message) {
        super(message);
    }

    public ClientExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientExistsException(Throwable cause) {
        super(cause);
    }

    public ClientExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
