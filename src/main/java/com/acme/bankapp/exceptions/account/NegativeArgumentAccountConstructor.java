package com.acme.bankapp.exceptions.account;

/**
 * Created by Kate on 20.04.2015.
 */
public class NegativeArgumentAccountConstructor extends Exception{
    public NegativeArgumentAccountConstructor() {
    }

    public NegativeArgumentAccountConstructor(String message) {
        super(message);
    }

    public NegativeArgumentAccountConstructor(String message, Throwable cause) {
        super(message, cause);
    }

    public NegativeArgumentAccountConstructor(Throwable cause) {
        super(cause);
    }

    public NegativeArgumentAccountConstructor(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
