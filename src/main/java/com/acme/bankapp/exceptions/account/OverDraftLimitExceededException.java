package com.acme.bankapp.exceptions.account;

import com.acme.bankapp.domain.account.Account;

/**
 * Created by Kate on 20.04.2015.
 */
public class OverDraftLimitExceededException extends NotEnoughFundsException {
    private double maxAmount;
    private Account account;

    public OverDraftLimitExceededException(double amount, Account account, double maxAmount) {
        super(amount);
        this.account = account;
        this.maxAmount = maxAmount;
    }

    public OverDraftLimitExceededException(String message, double amount, Account account, double maxAmount) {
        super(message, amount);
        this.account = account;
        this.maxAmount = maxAmount;
    }

    public OverDraftLimitExceededException(String message, Throwable cause, double amount, Account account, double maxAmount) {
        super(message, cause, amount);
        this.account = account;
        this.maxAmount = maxAmount;
    }

    public OverDraftLimitExceededException(Throwable cause, double amount, Account account, double maxAmount) {
        super(cause, amount);
        this.account = account;
        this.maxAmount = maxAmount;
    }

    public OverDraftLimitExceededException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, double amount, Account account, double maxAmount) {
        super(message, cause, enableSuppression, writableStackTrace, amount);
        this.account = account;
        this.maxAmount = maxAmount;
    }

    public double getMaxAmount() {
        return maxAmount;
    }

    public Account getAccount() {
        return account;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public double getAmount() {
        return super.getAmount();
    }

    @Override
    public void setAmount(double amount) {
        super.setAmount(amount);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(super.toString());
        stringBuilder.append("\n")
                .append("Client try to withdraw amount: ")
                .append(getAmount())
                .append(" from  ")
                .append(account);
        return stringBuilder.toString();
    }
}
