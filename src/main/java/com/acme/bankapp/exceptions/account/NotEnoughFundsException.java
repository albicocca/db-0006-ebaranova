package com.acme.bankapp.exceptions.account;

import com.acme.bankapp.exceptions.BankException;

/**
 * Created by Kate on 20.04.2015.
 */
public class NotEnoughFundsException extends BankException {
    private double amount;

    public NotEnoughFundsException(double amount) {
        this.amount = amount;
    }

    public NotEnoughFundsException(String message, double amount) {
        super(message);
        this.amount = amount;
    }

    public NotEnoughFundsException(String message, Throwable cause, double amount) {
        super(message, cause);
        this.amount = amount;
    }

    public NotEnoughFundsException(Throwable cause, double amount) {
        super(cause);
        this.amount = amount;
    }

    public NotEnoughFundsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, double amount) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
