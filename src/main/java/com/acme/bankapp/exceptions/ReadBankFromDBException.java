package com.acme.bankapp.exceptions;

/**
 * Created by Kate on 24.04.2015.
 */
public class ReadBankFromDBException extends BankException{
    public ReadBankFromDBException() {
        super();
    }

    public ReadBankFromDBException(String message) {
        super(message);
    }

    public ReadBankFromDBException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReadBankFromDBException(Throwable cause) {
        super(cause);
    }

    protected ReadBankFromDBException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
