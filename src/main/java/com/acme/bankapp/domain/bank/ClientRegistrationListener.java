package com.acme.bankapp.domain.bank;

import com.acme.bankapp.domain.client.Client;

import java.io.Serializable;

/**
 * Created by Kate on 17.04.2015.
 */
public interface ClientRegistrationListener{
    void onClientAdded(Client c);
}
