package com.acme.bankapp.domain.bank;

import com.acme.bankapp.apps.bank.services.EmailService;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.exceptions.SendEmailException;

import java.io.Serializable;

/**
 * Created by kate on 26.04.15.
 */
public class EmailNotificationListener implements ClientRegistrationListener, Serializable{
    EmailService emailService = new EmailService();
    @Override
    public void onClientAdded(Client c) {
        try {
            emailService.sendModificationEmail(c, "e-address", "Notification email for client " + c.getClientSalutation() + " to be sent");
        } catch (SendEmailException e) {
            System.err.println("Can' send email for client "+ c.getClientSalutation()+" because of: "+ e.getMessage());
        }
    }
}
