package com.acme.bankapp.domain.bank;

import com.acme.bankapp.domain.account.Account;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.apps.bank.services.EmailService;
import com.acme.bankapp.exceptions.SendEmailException;
import com.acme.bankapp.exceptions.client.ClientExistsException;
import java.io.*;
import java.util.*;

public class Bank implements Serializable {
    private List<Client> clients;
    private List<ClientRegistrationListener> listeners;

    public Bank(){
        listeners = new ArrayList<>();
        clients = new ArrayList<>();
        addListener(new PrintClientListener());
        addListener(new EmailNotificationListener());
    }


    public Bank(List<ClientRegistrationListener> listeners) {
        this();
        listeners.forEach(this::addListener);
    }
    public List<Client> getClients(){
        return Collections.unmodifiableList(clients);
    }

    public void addClient(Client client) throws ClientExistsException {
        if (clients.contains(client)){
            throw new ClientExistsException("Client with name "+client.getName()+" already exists");
        }
        clients.add(client);
        for (ClientRegistrationListener listener: listeners){
            listener.onClientAdded(client);
        }
    }

    public List<ClientRegistrationListener> getListeners() {
        return Collections.unmodifiableList(listeners);
    }

    public void addListener(ClientRegistrationListener listener){
        if (!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    public void removeListener(ClientRegistrationListener listener){
        listeners.remove(listener);
    }


}
