package com.acme.bankapp.domain.bank;

import com.acme.bankapp.domain.account.Account;
import com.acme.bankapp.domain.client.Client;

import java.io.Serializable;

/**
 * Created by kate on 26.04.15.
 */

public class PrintClientListener implements ClientRegistrationListener, Serializable {
    @Override
    public void onClientAdded(Client client) {
        System.out.print(client.getClientSalutation()+" ");
        Account accounts = client.getAccounts();
        if (accounts != null) {
            System.out.print(accounts.getBalance() + " ");
            System.out.print(accounts.maximumAmountToWithdraw());
        }
        System.out.println( " from thread: "+ Thread.currentThread().getName());

    }
}
