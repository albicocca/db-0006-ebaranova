package com.acme.bankapp.domain.client;


import java.io.Serializable;

public enum Gender implements Serializable{
    MALE("Mr."), FEMALE("Ms.");

    private final String salutation;

    Gender(String salutation) {
        this.salutation = salutation;
    }
     public String getSalutation(){
         return salutation;
     }
}
