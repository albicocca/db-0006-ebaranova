package com.acme.bankapp.domain.client;

import com.acme.bankapp.domain.account.Account;

import java.io.Serializable;

public class Client implements Serializable {
    private Account accounts;   //now we consider that client can hav only one account
    private String name;
    private Gender gender;

    public Client(String name, Gender gender) {
        this.name = name;
        this.gender = gender;
    }

    public Client(String name, Gender gender, Account account) {
        this(name,gender);
        accounts = account;

    }

    public Account getAccounts() {
        return accounts;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    //really this is not add, but set, while our client can have only one account.
    public void addAccount(Account account) {
        this.accounts =account;
    }

    public String getClientSalutation(){
        return gender.getSalutation()+" "+ name;
    }

    @Override
    public int hashCode() {
        return name.hashCode(); //пока считаем, что name -  что-то вроде id
    }

    @Override
    public boolean equals(Object obj) {
        if (name == null){
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Client)) {
            return false;
        }
        return this.name.equals(((Client) obj).getName());
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Client ");
        stringBuilder.append(getClientSalutation());
        if (accounts != null) {
            stringBuilder.append(" with account: ");
            stringBuilder.append(accounts.toString());
        } else {
            stringBuilder.append(" without account.");
        }
        return stringBuilder.toString();
    }
}
