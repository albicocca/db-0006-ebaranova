package com.acme.bankapp.domain.account;

import com.acme.bankapp.exceptions.account.NegativeArgumentAccountConstructor;
import com.acme.bankapp.exceptions.account.NotEnoughFundsException;

/**
 * Created by kate on 16.04.15.
 */
public abstract class AbstractAccount implements Account {
    private double balance;

    public AbstractAccount(double balance) throws NegativeArgumentAccountConstructor {
        if (balance < 0) {
            throw new NegativeArgumentAccountConstructor("Balance can't be negative");
        }
        this.balance = balance;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    //change here double to Double just to show using of autoboxing
    protected void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public abstract void deposit(double mount);

    @Override
    public abstract void withdraw(double mount) throws NotEnoughFundsException;

    @Override
    public abstract double maximumAmountToWithdraw();

    @Override
    public abstract long decimalValue();
}
