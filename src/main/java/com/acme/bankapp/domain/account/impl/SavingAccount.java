package com.acme.bankapp.domain.account.impl;

import com.acme.bankapp.domain.account.AbstractAccount;
import com.acme.bankapp.exceptions.account.NegativeArgumentAccountConstructor;
import com.acme.bankapp.exceptions.account.NotEnoughFundsException;

public class SavingAccount extends AbstractAccount {
    public SavingAccount(double balance) throws NegativeArgumentAccountConstructor {
        super(balance);
    }

    @Override
    public void deposit(double mount) {
        super.setBalance(super.getBalance()+mount);
    }

    @Override
    public void withdraw(double mount) throws NotEnoughFundsException {
        double balance = getBalance();
        if (mount <= balance){
            setBalance(balance - mount);
        } else{
            throw new NotEnoughFundsException("Client don't have enough money",mount);
        }
    }

    @Override
    public double maximumAmountToWithdraw() {
        return getBalance();
    }

    @Override
    public long decimalValue() {
        return Math.round(getBalance());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SavingAccount)) return false;

        SavingAccount that = (SavingAccount) o;

        return Double.compare(that.getBalance(), getBalance()) == 0;

    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(getBalance());
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        return "Account ("+this.getClass().getSimpleName()+") with balance = "+getBalance();
    }
}
