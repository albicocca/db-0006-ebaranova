package com.acme.bankapp.domain.account.impl;

import com.acme.bankapp.domain.account.AbstractAccount;
import com.acme.bankapp.exceptions.account.NegativeArgumentAccountConstructor;
import com.acme.bankapp.exceptions.account.OverDraftLimitExceededException;

public class CheckingAccount extends AbstractAccount {
    private double overdraft;

    public CheckingAccount(double balance) throws NegativeArgumentAccountConstructor {
        super(balance);
        overdraft = 0;
    }

    public CheckingAccount(double balance, double overdraft) throws NegativeArgumentAccountConstructor {
        super(balance);
        if (overdraft < 0){
            throw new NegativeArgumentAccountConstructor("Can't be negative overdraft");
        }
        this.overdraft = overdraft;
    }

    @Override
    public void deposit(double mount) {
        super.setBalance(super.getBalance()+mount);
    }

    public double getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(double overdraft) {
        this.overdraft = overdraft;
    }

    @Override
    public void withdraw(double mount) throws OverDraftLimitExceededException {
        double balance = getBalance();
        if (mount <= balance){
            setBalance(balance - mount);
        } else if(mount <= (balance +overdraft)){
            overdraft = (overdraft + balance) - mount;
            setBalance(0.0);
        } else {
            throw new OverDraftLimitExceededException(
                    "Client don't have enough money even with overdraft",
                    mount,
                    this,
                    maximumAmountToWithdraw());
        }
        assert overdraft>=0: "Overdraft is negative&!";
        assert super.getBalance()>=0: "Balance is negative&!";
    }

    @Override
    public double maximumAmountToWithdraw() {
        return getBalance()+overdraft;
    }

    @Override
    public long decimalValue() {
        return Math.round(getBalance());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CheckingAccount)) return false;

        CheckingAccount that = (CheckingAccount) o;

        if (Double.compare(that.overdraft, overdraft) != 0) return false;
        if (Double.compare(that.getBalance(),getBalance())!=0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(overdraft);
        return (int) (temp ^ (temp >>> 32));
    }

    @Override
    public String toString() {
        return "Account ("+this.getClass().getSimpleName()+") with balance = "+getBalance()+ " and overdraft = "+ overdraft;
    }
}
