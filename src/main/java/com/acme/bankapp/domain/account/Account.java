package com.acme.bankapp.domain.account;

import com.acme.bankapp.exceptions.account.NotEnoughFundsException;

import java.io.Serializable;

public interface Account extends Serializable {
    double getBalance();
    void deposit(double mount);
    void withdraw(double mount) throws NotEnoughFundsException;
    double maximumAmountToWithdraw();
    long decimalValue();
}
