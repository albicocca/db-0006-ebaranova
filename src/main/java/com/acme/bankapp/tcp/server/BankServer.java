package com.acme.bankapp.tcp.server;

import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.exceptions.BankException;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * Created by kate on 21.04.15.
 */
public class BankServer {
    public void start(Bank bank) throws BankException, ReadBankFromFileException {
        try (ServerSocket serverSocket = new ServerSocket(9988, 10)){
            serverSocket.setSoTimeout(5000000);
            Socket connection;
            while (true) {
                connection = serverSocket.accept();
                new OneClientThread(connection, bank).start();
            }
        } catch (SocketTimeoutException e){
            throw new BankException("Server shut down after time out");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


