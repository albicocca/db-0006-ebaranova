package com.acme.bankapp.tcp.server;

import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;
import com.acme.bankapp.exceptions.client.ClientExistsException;
import com.acme.bankapp.apps.bank.services.BankService;

/**
 * Created by kate on 21.04.15.
 */
public class BankReport {
    public String fullInfo(Bank bank){
        StringBuilder stringBuilder = new StringBuilder();
        for (Client client: bank.getClients()){
            stringBuilder.append(client.toString());
            stringBuilder.append("++");
        }
        return stringBuilder.toString();
    }

    public String shortInfo(Bank bank){
        return "Bank has " + bank.getClients().size() + " clients";
    }

    //TODO: test, it don't work correctly
    public String addClient(Bank bank, String client) {
        try {
            new BankService().addClient(bank,client);
            return "Add new client to bank. Test it with full info.";
        } catch (ReadBankFromFileException e) {
            e.printStackTrace();
        } catch (ClientExistsException e) {
            e.printStackTrace();
        }
        return "Can't add client";
    }
}
