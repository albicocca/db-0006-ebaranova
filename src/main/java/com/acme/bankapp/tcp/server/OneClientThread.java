package com.acme.bankapp.tcp.server;

import com.acme.bankapp.domain.bank.Bank;

import java.io.*;
import java.net.Socket;

/**
 * Created by Kate on 23.04.2015.
 */
public class OneClientThread extends Thread{
    private Socket connection;
    private Bank bank;

    public OneClientThread(Socket connection, Bank bank) {
        this.connection = connection;
        this.bank = bank;
    }

    @Override
    public void run() {
        try (OutputStream out = connection.getOutputStream();
             InputStream in = connection.getInputStream()){

            BankReport bankReport = new BankReport();
            String command;

            do {
                command = readMessage(in);
                System.out.println("Get command '" + command + "'"+" from thread "+ Thread.currentThread().getName());
                //TODO: do strategy
                switch (command) {
                    case "full":
                        sendMessage(bankReport.fullInfo(bank), out);
                        break;
                    case "short":
                        sendMessage(bankReport.shortInfo(bank), out);
                        break;
                    //TODO: test, it don't work correctly
                    case "addClient":
                        sendMessage("Please, send string representation of client:", out);
                        String clientString = readMessage(in);
                        sendMessage(bankReport.addClient(bank, clientString), out);
                        break;
                    case "bye":
                        sendMessage("Bye from server!", out);
                        break;
                    default:
                        sendMessage("Unknown command", out);
                }
            } while ((!"bye".equals(command)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(String message, OutputStream out) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(out));
        bufferedWriter.write(message);
        bufferedWriter.newLine();
        bufferedWriter.flush();
    }

    private String readMessage(InputStream in) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        return  bufferedReader.readLine();
    }
}
