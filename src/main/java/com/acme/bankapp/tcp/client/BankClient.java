package com.acme.bankapp.tcp.client;

import java.io.*;
import java.net.Socket;

/**
 * Created by kate on 21.04.15.
 */
public class BankClient {
    private Socket connection;
    private OutputStream out;
    private InputStream in;

    public void run(){
        try {
            connection = new Socket("localhost",9988);
            out = connection.getOutputStream();
            in = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String command;
            String response;
            do {
                command = bufferedReader.readLine();
                sendMessage(command);
                response=readMessage();
                System.out.println(response.replace("++","\n"));
            } while (!"bye".equals(command));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try{
                in.close();
                out.close();
                connection.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void sendMessage(String command) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(out));
        bufferedWriter.write(command);
        bufferedWriter.newLine();
        bufferedWriter.flush();
    }

    private String readMessage() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        return  bufferedReader.readLine();
    }

    public static void main(String... args){
        new BankClient().run();
    }
}
