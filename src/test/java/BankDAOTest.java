import com.acme.bankapp.domain.bank.Bank;
import com.acme.bankapp.exceptions.BankException;
import com.acme.bankapp.apps.bank.loaders.BankDAO;
import com.acme.bankapp.apps.bank.services.BankService;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.fest.assertions.Assertions.*;

/**
 * Created by Kate on 24.04.2015.
 */
public class BankDAOTest {

    @Test
    public void shouldReturnBankWhenLoadFromDB() throws BankException {
        BankService sut = new BankService();
        BankDAO stub = mock(BankDAO.class);
        Bank dummy = new Bank();
        when(stub.loadBank()).thenReturn(dummy);
        assertThat(sut.loadBank(stub)).isSameAs(dummy);
    }


}
