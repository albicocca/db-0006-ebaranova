import com.acme.bankapp.exceptions.BankException;
import com.acme.bankapp.apps.bank.loaders.FeedStringClientLoader;
import com.acme.bankapp.apps.bank.loaders.FileBankDAO;
import com.acme.bankapp.apps.bank.loaders.StringClientLoader;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.mockito.Mockito.*;

/**
 * Created by kate on 26.04.15.
 */
public class FileBankDAOTest {

    @Test
    public void shouldCallLoadClientWhenLoadBank() throws FileNotFoundException, BankException {
        FileBankDAO sut = new FileBankDAO("feed");
        int COUNT_LINES_IN_FEED = 4;

        StringClientLoader spy = spy(new FeedStringClientLoader());
        sut.setStringClientLoader(spy);
        sut.loadBank();

        verify(spy, times(COUNT_LINES_IN_FEED)).loadClient(anyString());
    }

}
