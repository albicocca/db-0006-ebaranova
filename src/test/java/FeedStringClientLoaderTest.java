import com.acme.bankapp.domain.account.impl.CheckingAccount;
import com.acme.bankapp.domain.account.impl.SavingAccount;
import com.acme.bankapp.domain.client.Client;
import com.acme.bankapp.domain.client.Gender;
import com.acme.bankapp.exceptions.account.NegativeArgumentAccountConstructor;
import com.acme.bankapp.exceptions.bank.ReadBankFromFileException;
import com.acme.bankapp.apps.bank.loaders.FeedStringClientLoader;
import org.junit.Test;

import static org.fest.assertions.Assertions.*;


/**
 * Created by kate on 26.04.15.
 */
public class FeedStringClientLoaderTest {

    @Test
    public void shouldReturnMaleClientWhenMaleAndFullLine() throws ReadBankFromFileException {
        String stub = "accounttype=c;balance=1000;overdraft=50000;name=John;gender=m";
        FeedStringClientLoader sut = new FeedStringClientLoader();

        Client client = sut.loadClient(stub);
        assertThat(client.getGender()).isEqualTo(Gender.MALE);
    }

    @Test
    public void shouldReturnFemaleMaleClientWhenFemaleAndFullLine() throws ReadBankFromFileException {
        String stub = "accounttype=c;balance=1000;overdraft=50000;name=John;gender=f";
        FeedStringClientLoader sut = new FeedStringClientLoader();

        Client client = sut.loadClient(stub);

        assertThat(client.getGender()).isEqualTo(Gender.FEMALE);
    }

    @Test
    public void shouldThrowExceptionWhenWrongGender() {
        String stub = "accounttype=c;balance=1000;overdraft=50000;name=John;gender=wtf";
        FeedStringClientLoader sut = new FeedStringClientLoader();
        try {
            Client client = sut.loadClient(stub);
        } catch (ReadBankFromFileException e) {
            assertThat(e).isInstanceOf(ReadBankFromFileException.class)
                    .hasMessage("Unknown gender in string: " + stub);
        }
    }

    @Test
    public void shouldCreateCheckAccountWhenLineGetC() throws ReadBankFromFileException {
        String stub = "accounttype=c;balance=1000;overdraft=50000;name=John;gender=m";
        FeedStringClientLoader sut = new FeedStringClientLoader();
        Client client = sut.loadClient(stub);
        assertThat(client.getAccounts()).isInstanceOf(CheckingAccount.class);
    }

    @Test
    public void shouldCreateSavingAccountWhenLineGetS() throws ReadBankFromFileException {
        String stub = "accounttype=s;balance=1000;overdraft=50000;name=John;gender=m";
        FeedStringClientLoader sut = new FeedStringClientLoader();
        Client client = sut.loadClient(stub);
        assertThat(client.getAccounts()).isInstanceOf(SavingAccount.class);
    }

    @Test
    public void shouldCreateBalance1000WhenLineGetB1000() throws ReadBankFromFileException {
        String stub = "accounttype=s;balance=1000;overdraft=50000;name=John;gender=m";
        FeedStringClientLoader sut = new FeedStringClientLoader();
        Client client = sut.loadClient(stub);
        assertThat(client.getAccounts().getBalance()).isEqualTo(1000.0);
    }

    @Test
    public void shouldThrowExcepitionWhenLineGetNegativeBalance() {
        String stub = "accounttype=s;balance=-1000;overdraft=50000;name=John;gender=m";
        FeedStringClientLoader sut = new FeedStringClientLoader();
        try {
            Client client = sut.loadClient(stub);
        } catch (ReadBankFromFileException e) {
            assertThat(e).isInstanceOf(ReadBankFromFileException.class);
            assertThat(e.getCause()).isInstanceOf(NegativeArgumentAccountConstructor.class);
        }
    }

    @Test
    public void shouldThrowExcepitionWhenLineGetNegativeOverdraft() {
        String stub = "accounttype=c;balance=1000;overdraft=50000;name=John;gender=m";
        FeedStringClientLoader sut = new FeedStringClientLoader();
        try {
            Client client = sut.loadClient(stub);
        } catch (ReadBankFromFileException e) {
            assertThat(e).isInstanceOf(ReadBankFromFileException.class);
            assertThat(e.getCause()).isInstanceOf(NegativeArgumentAccountConstructor.class);
        }
    }

    @Test
    public void shouldThrowExcepitionWhenLineDontHaveAtype() {
        String stub = "balance=1000;overdraft=-50000;name=John;gender=m";
        FeedStringClientLoader sut = new FeedStringClientLoader();
        try {
            Client client = sut.loadClient(stub);
        } catch (ReadBankFromFileException e) {
            assertThat(e).isInstanceOf(ReadBankFromFileException.class);
            assertThat(e.getCause()).isInstanceOf(NullPointerException.class);
        }
    }



}
